package com.omi.tacdogs.logger;

import com.ngc.sf2.event.EventListener;
import edu.jhuapl.tacdogs.api.event.SampleResultEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TacdogEventListener implements EventListener<SampleResultEvent> {

    private static final Logger logger = LoggerFactory.getLogger(TacdogEventListener.class);

    public void onEvent(SampleResultEvent event) {
        logger.info("tacdog SampleResultEvent: {}", event.toString());
    }
}
