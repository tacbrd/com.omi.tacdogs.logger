package com.omi.tacdogs.logger;

import com.ngc.sf2.event.*;
import edu.jhuapl.tacdogs.api.event.SampleResultEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AllEventListener implements EventListener<Event> {

    private static final Logger logger = LoggerFactory.getLogger(AllEventListener.class);

    public void onEvent(Event event) {
        logger.info("AllEventListener Event: {}", event.toString());
    }
}
