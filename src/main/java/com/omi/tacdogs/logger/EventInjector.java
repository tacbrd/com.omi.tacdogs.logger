package com.omi.tacdogs.logger;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ngc.sf2.event.EventService;

public class EventInjector {

    private static final Logger logger = LoggerFactory.getLogger(EventInjector.class);

	private EventService eventService;
	
	public EventInjector() {
		logger.info("create EventInjector");
		while(true) {
			sendEvent();
			// wait 30 seconds
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void sendEvent() {
		if(eventService != null) {
				// inject
				logger.info("Sending an event");
				eventService.publish(new MyEvent());
				
			
		} else {
			logger.info("EventInjector eventService is null");
		}
	}

	public void setEventService(
			EventService eventService) {
		logger.info("setEventService: " + eventService);
		this.eventService = eventService;
	}

}
