package com.omi.tacdogs.logger;

import com.ngc.sf2.event.Event;

public class MyEvent implements Event {

	@Override
	public String toString() {
		return "This is my event";
	}
}
